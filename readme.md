**Java Rest**
-------------

My first project using [Jersey 2.x](https://jersey.java.net/), an implementation of the [JAX-RS Java API](https://jax-rs-spec.java.net/) for [RESTful web services](https://en.wikipedia.org/wiki/Java_API_for_RESTful_Web_Services).

The project is based on the [Maven webapp archetype](http://maven.apache.org/archetypes/maven-archetype-webapp/):

```
mvn archetype:generate -DgroupId=com.example.web -DartifactId=java-rest \
    -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false
```

*TODOs*

* Authentication+authorization
* Database integration
* ++
