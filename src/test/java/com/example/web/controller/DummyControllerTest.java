package com.example.web.controller;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import static org.junit.Assert.assertEquals;

/**
 * Created by flesk on 2016-09-22.
 */
public class DummyControllerTest extends JerseyTest {

    // This is the example from https://jersey.java.net/documentation/latest/test-framework.html

    private static class Dummy {
        public String greeting;
        public String person;

        @JsonCreator
        public Dummy(@JsonProperty("greeting") String greeting,
                     @JsonProperty("person") String person) {
            this.greeting = greeting;
            this.person = person;
        }
    }

    @Path("hello")
    public static class HelloResource {
        @GET
        public String getHello() {
            return "Hello, World!";
        }
        @POST
        @Produces(MediaType.TEXT_HTML)
        public String postGreeting(Dummy dummy) {
            return dummy.greeting + ", " + dummy.person + "!";
        }
    }

    @Override
    protected Application configure() {
        return new ResourceConfig(HelloResource.class);
    }

    @Test
    public void testGet() {
        final String greeting = target("hello").request().get(String.class);
        assertEquals("Hello, World!", greeting);
    }

    @Test
    public void testPost() {
        Dummy dummy = new Dummy("Hello", "human");
        Entity<Dummy> dummyEntity = Entity.entity(dummy, MediaType.APPLICATION_JSON_TYPE);
        final String greeting = target("hello").request().post(dummyEntity).readEntity(String.class);
        assertEquals("Hello, human!", greeting);
    }

}
