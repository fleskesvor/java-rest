package com.example.web.controller;

import com.example.web.entity.Customer;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.internal.inject.Custom;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.core.Application;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by flesk on 2016-09-22.
 */
public class CustomerControllerTest extends JerseyTest {

    // TODO: Make sure each endpoint is covered

    @Override
    protected Application configure() {
        return new ResourceConfig(CustomerController.class);
    }

    @Test
    public void testStatus() {
        final String status = target("customers/status").request().get(String.class);
        assertEquals("CustomerController responding", status);
    }

    @Test
    public void testGetCustomer() {
        final Customer customer = target("customers/1234").request().get(Customer.class);
        assertNotNull(customer);
    }

    @Test
    public void testGetCustomerList() throws IOException {
        final String customerString = target("customers").request().get(String.class);
        final ObjectMapper mapper = new ObjectMapper();
        final TypeReference typeReference = new TypeReference<List<Customer>>() {};
        List<Customer> customerList;

        try {
            customerList = mapper.readValue(customerString, typeReference);
        } catch (IOException e) {
            throw new AssertionError(e);
        }

        assertNotNull(customerList);
        assertTrue(customerList.size() > 0);
    }
}
