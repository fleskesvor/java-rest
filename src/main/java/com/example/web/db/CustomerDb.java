package com.example.web.db;

import com.example.web.entity.Customer;
import com.example.web.entity.PhoneNumber;

import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by flesk on 2016-09-22.
 */
@Provider
public class CustomerDb {
    /*
        Temporary DB replacement
     */

    private static List<Customer> customers;

    static {
        customers = new ArrayList<Customer>();
        List<PhoneNumber> phoneNumberList = new ArrayList<PhoneNumber>();
        phoneNumberList.add(new PhoneNumber(67L, "99999999", "Testnummer"));
        customers.add(new Customer(1234L, "Fleskemanden & EFTF", null, null, null, phoneNumberList));
        customers.add(new Customer(9138L, "Pats Spesialpakkeservice"));
        Calendar cal = Calendar.getInstance();
        cal.set(1990, 1, 15);
        Date startDate = cal.getTime();
        cal.add(Calendar.YEAR, 25);
        cal.add(Calendar.MONTH, 6);
        Date endDate = cal.getTime();
        customers.add(new Customer(4257L, "Ibsens Isenkram", "468 77 864", startDate, endDate, null));
    }

    public static List<Customer> getCustomerList() {
        return customers;
    }

    public Customer getCustomerById(long id) {
        for (Customer customer : customers) {
            if (customer.getId() == id) {
                return customer;
            }
        }
        return null;
    }

    public List<Customer> getCustomerList(String searchString) {
        searchString = searchString.toLowerCase();
        List<Customer> searchResult = new ArrayList<Customer>();
        for (Customer customer : customers) {
            String customerName = customer.getCustomerName().toLowerCase();
            if (customerName.contains(searchString))
                searchResult.add(customer);
        }
        return searchResult;
    }
}
