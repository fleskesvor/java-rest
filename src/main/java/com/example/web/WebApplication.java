package com.example.web;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

/**
 * Created by flesk on 2016-09-22.
 */
@ApplicationPath("/")
public class WebApplication extends ResourceConfig {
    public WebApplication() {
        System.out.println("Configuring ResourceConfig...");
        /*
            We can use package scanning to provide a single interface
            for DB and have several implementations.
        */
        packages(true, "com.example.web");
    }
}
