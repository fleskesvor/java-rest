package com.example.web.service;

import com.example.web.db.CustomerDb;
import com.example.web.entity.Customer;
import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by flesk on 2016-09-22.
 */
@Service
public class CustomerService {
    /*
        Query CustomerDb, etc.
     */

    @Autowired
    CustomerDb customerDb;

    // Make setter available to Spring
    public void setCustomerDb(CustomerDb customerDb) {
        this.customerDb = customerDb;
    }

    public Customer getCustomerById(long id) {
        return customerDb.getCustomerById(id);
    }
}
