package com.example.web.controller;

import com.example.web.db.CustomerDb;
import com.example.web.entity.Customer;
import com.example.web.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Created by flesk on 2016-09-22.
 */
@Path("customers")
public class CustomerController {

    @Context
    SecurityContext securityContext;

    @Autowired
    CustomerService customerService;

    // Make setter available to Spring
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GET @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerList() {
        return Response.ok(CustomerDb.getCustomerList()).build();
    }

    @Path("/status")
    @GET @Produces(MediaType.TEXT_HTML)
    public String getStatus() {
        return "CustomerController responding";
    }

    @Path("{number}")
    @GET @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomer(@PathParam("number") long id) {
        Customer customer = customerService.getCustomerById(id);
        return Response.ok(customer).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomer(Customer customer) {
        System.out.println("Creating new Customer with POST " + customer);
        return Response.ok(customer).build();
    }
}
