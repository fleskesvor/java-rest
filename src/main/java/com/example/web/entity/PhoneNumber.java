package com.example.web.entity;

/**
 * Created by flesk on 2016-09-22.
 */
public class PhoneNumber {
    Long id;
    String number;
    String description;

    public PhoneNumber() {
    }

    public PhoneNumber(Long id, String number, String description) {
        this.id = id;
        this.number = number;
        this.description = description;
    }

    public String getNumber() {
        return number;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "PhoneNumber{" +
                ", number='" + number + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
