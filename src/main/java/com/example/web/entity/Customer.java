package com.example.web.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by flesk on 2016-09-22.
 */
public class Customer implements Serializable {
    Long id;
    String customerName;
    String contactNumber;
    Date startDate;
    Date endDate;
    List<PhoneNumber> phoneNumberList;

    public Customer() {
    }

    public Customer(Long id, String customerName) {
        this.id = id;
        this.customerName = customerName;
    }

    public Customer(Long id, String customerName, String contactNumber, Date startDate, Date endDate, List<PhoneNumber> phoneNumberList) {
        this.id = id;
        this.customerName = customerName;
        this.contactNumber = contactNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        this.phoneNumberList = phoneNumberList;
    }

    public Long getId() {
        return id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public List<PhoneNumber> getPhoneNumberList() {
        return phoneNumberList;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", phoneNumberList=" + phoneNumberList +
                ", customerName='" + customerName + '\'' +
                '}';
    }
}
